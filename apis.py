import requests,os
def cep():
	os.system('cls' if os.name == 'nt' else 'clear')
	cep = input("Insira o CEP: ")
	req = requests.get(f"https://viacep.com.br/ws/{cep}/json/").json()
	print(f'''
	\033[1;33m	
	CEP: {cep}
	Rua: {req['logradouro']}
	Complemento: {req['complemento']}
	Bairro: {req['bairro']}
	Localidade: {req['localidade']}
	Estado: {req['uf']}
	Ibge: {req['ibge']}
	Gia: {req['gia']}
	DDD: {req['ddd']}
	Siafi: {req['siafi']}
''')
def cnpj():
    os.system('cls' if os.name == 'nt' else 'clear')
    cnpj = input("Insira o CNPJ: ")
    req = requests.get(f"https://www.receitaws.com.br/v1/cnpj/{cnpj}").json()
    for key, value in req.items():
        print(f'{key}: {value}:')
def ip():
     os.system('cls' if os.name == 'nt' else 'clear')
     ip_inpu = input('Insira o IP: ')
     req_ip = requests.get(f"http://ip-api.com/json/{ip_inpu}").json()
     print(f'''
            Resultados 
========================================
\033[1;33m
          [+] ENCONTRADO
          País: {req_ip['country']}
          Região: {req_ip['regionName']}
          Cidade: {req_ip['city']}
          Provedor: {req_ip['org']}
          Fuso-Horário: {req_ip['timezone']}
''')

def placa():
	os.system('cls' if os.name == 'nt' else 'clear')
	placa_inpu = input("Insira a placa: ")
	req_placa = requests.get(f"https://apicarros.com/v1/consulta/{placa_inpu}/json",verify=False).json()
	os.system('cls' if os.name == 'nt' else 'clear')
	print(f'''
	Placa > {placa_inpu}
	Ano > {req_placa['ano']}
	Chassi > {req_placa['chassi']}
	Cor > {req_placa['cor']}
	Data > {req_placa['data']}
	Data atualização caracteristicas do veiculo > {req_placa['dataAtualizacaoCaracteristicasVeiculo']}
	Data atualização roubo/furto > {req_placa['dataAtualizacaoRouboFurto']}
	Marca > {req_placa['marca']}
	Modelo > {req_placa['modelo']}
	Municipio > {req_placa['municipio']}
	Situação > {req_placa['situacao']}
	UF > {req_placa['uf']}
    	''')